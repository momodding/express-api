var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/users', function (req, res, next) {
  var resp = { status: true, message: 'API: Get users' };
  res.json(resp);
});

module.exports = router;
