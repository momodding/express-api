var express = require('express');
var response = require('../config/res');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  // res.render('index', { title: 'Express' });
  // var resp = { status: true, message: 'hooray! welcome to our api!' };
  // res.json(resp);
  response.ok('hooray! welcome to our api!', 'sukses', res);
});

module.exports = router;
// 'use strict';
// exports.index = function (req, res) {
//   response.ok('hooray! welcome to our api!', 'sukses', res);
// }
