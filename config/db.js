var mysql = require('mysql');

const conn = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'express-api'
});

module.exports = conn;