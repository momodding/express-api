'use strict';

exports.ok = function (val, msg, res) {
  var data = {
    'status': 200,
    'message': msg,
    'result': val
  };
  res.status(200).json(data);
  res.end();
};

exports.error = function (val, msg, res) {
  var data = {
    'status': 500,
    'message': msg,
    'result': val
  };
  res.status(500).json(data);
  res.end();
};

exports.created = function (val, msg, res) {
  var data = {
    'status': 201,
    'message': msg,
    'result': val
  };
  res.status(201).json(data);
  res.end();
};

exports.no_content = function (val, msg, res) {
  var data = {
    'status': 204,
    'message': msg,
    'result': val
  };
  res.status(204).json(data);
  res.end();
};